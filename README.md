# README #

A very simple web-based todo list.

## Requirements ##

* Java 1.8
* npm
* Maven

## Design ##

The assignment consists of two projects: api (backend) and frontend (web interface).

## api ##

api is a spring boot based rest api with h2 as in memory data store. 

### SETUP ###

Download the project to a desired location on your system.

### RUN ###

cd api

mvn package

java -jar target/todoapi-0.1.0.jar

URL: localhost:8080/tasks

**NOTE: project can be imported to eclipse.**

## FrontEnd ##
Front end is a very simple web based to do list.

### SETUP ###

* Download the project to a desired location on your system.
* cd frontend
* npm install

### RUN ###

cd frontend

ng serve --open

## Contact:##
In case of problems in running the project or other questions please do not hesitate to either email nadeemmugahl@hotmail.com or call +46706225633