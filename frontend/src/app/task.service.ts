import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import { Task } from './task';

@Injectable()
export class TaskService {

	private baseUrl: string = 'http://localhost:8080/';
	constructor(private http : Http){}

	getTasks(): Observable<Task[]> {
		let tasks$ = this.http
		.get(`${this.baseUrl}/tasks`, {headers: this.getHeaders()})
		.map(mapTasks);
		return tasks$;
	}

	addTask(task:Task): Observable<Task> {
		let options = new RequestOptions({ headers: this.getHeaders() });

		return this.http
		.post(`${this.baseUrl}/tasks`, task, options)
		.map(this.extractData)
		.catch(this.handleErrorObservable);
	}

	deleteTask(task:Task): Observable<Task> {
		return this.http
        .delete(`${this.baseUrl}/tasks` + "/" + task.id, {headers: this.getHeaders()})
        .map(this.extractData)
        .catch(this.handleErrorObservable);
	} 

	private getHeaders(){
		let headers = new Headers();
		headers.append('Accept', 'application/json');
		return headers;
	}

	private extractData(res: Response) {
		let body = res.json();
		return body || {};
	}

	private handleErrorObservable (error: Response | any) {
		return Observable.throw(error.message || error);
	}
}



function mapTasks(response:Response): Task[]{
	return response.json()._embedded.tasks.map(toTask)
}

function toTask(r:any): Task{
	let task = <Task>({
		id: Number.parseInt(r.id),
		title: r.title,
		completed: r.completed,
	});
	return task;
}
