import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Task } from './task';
import { TaskService } from './task.service';

@Component({
	selector: 'app-root',
	providers: [TaskService],
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
	template:  `

	<h1>My ToDos</h1>
	
	<input [(ngModel)]="newTask.title" placeholder="What to do!"> <button (click)="addTask(newTask)">Add Task!</button>

	<input type="checkbox" [checked]="showCompleted" (change)="showCompleted = !showCompleted " value="Show Completed"> show completed

<br><br>
	<table >

	<div *ngFor="let task of tasks" class="tasks">
    <tr>
    </tr>
    <tr *ngIf="(showCompleted && (task.completed || !task.completed) ) || (!showCompleted && !task.completed)">
       <td width="10"><input type="checkbox" [checked]="task.completed" (change)="updateTask(task)" /></td>
       <td width="500">{{task.title}}</td>
       <td width="10"><button (click)="deleteTask(task)" class="close">X</button></td>
    </tr>
    </div>
    </table>
	
	`
})

export class AppComponent implements OnInit{

	
	title = 'Task List';
	newTask = new Task();

	constructor(private taskService : TaskService){ }

	tasks: Task[];
	showCompleted: boolean;
	errorMessage: string;

	ngOnInit(){
		
		this.fetchTasks();
	}

	fetchTasks(): void {
       this.taskService
		.getTasks()
		.subscribe(
			/* happy path */  tasks => this.tasks = tasks,
			/* error path */ e => this.errorMessage = e);  
   }



	addTask(task: Task): void {
		this.taskService.addTask(task)
		.subscribe( task => {
			this.fetchTasks();		
		},
		error => this.errorMessage = <any>error);
	}

	updateTask(task: Task): void {
		task.completed = !task.completed;
		this.taskService.addTask(task)
		.subscribe( task => {
			this.fetchTasks();		
		},
		error => this.errorMessage = <any>error);
	}
	deleteTask(task: Task): void {
		this.taskService.deleteTask(task)
		.subscribe( task => {
			this.fetchTasks();		
		},
		error => this.errorMessage = <any>error);
	}
}