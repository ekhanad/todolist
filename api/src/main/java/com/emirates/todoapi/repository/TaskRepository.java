package com.emirates.todoapi.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.emirates.todoapi.resource.Task;

@RepositoryRestResource(collectionResourceRel = "tasks", path = "tasks")
 interface TaskRepository extends PagingAndSortingRepository<Task, Long> {

}

